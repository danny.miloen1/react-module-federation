const path = require('path');
const webpack = require('webpack');
const { ModuleFederationPlugin } = require('webpack').container;
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const Dotenv = require('dotenv-webpack');
const InterpolateHtmlPlugin = require('react-dev-utils/InterpolateHtmlPlugin');
const getCSSModuleLocalIdent = require('react-dev-utils/getCSSModuleLocalIdent');

const NodeAsyncHttpRuntime = require('./plugins/module-federation/NodeAsyncHTTPRuntime');

const webpackProgressHandler = require('./scripts/utils/webpackProgress');
const packageJson = require('./package.json');

const { NODE_ENV, GENERATE_SOURCEMAP, PUBLIC_URL, DEV_SERVER } = process.env;

const isEnvDevelopment = NODE_ENV !== 'production';
const isEnvProduction = NODE_ENV === 'production';
const isDevServer = DEV_SERVER === 'true';
const shouldUseSourceMap = GENERATE_SOURCEMAP !== 'false';

const cssRegex = /\.css$/;
const cssModuleRegex = /\.module\.css$/;

// common function to get style loaders
const getStyleLoaders = (cssOptions) => {
  const loaders = [
    isEnvDevelopment && require.resolve('style-loader'),
    isEnvProduction && {
      loader: MiniCssExtractPlugin.loader,
    },
    {
      loader: require.resolve('css-loader'),
      options: cssOptions,
    },
    {
      // Options for PostCSS as we reference these options twice
      // Adds vendor prefixing based on your specified browser support in
      // package.json
      loader: require.resolve('postcss-loader'),
      options: {
        postcssOptions: {
          // Necessary for external CSS imports to work
          // https://github.com/facebook/create-react-app/issues/2677
          ident: 'postcss',
          config: false,
          plugins: [
            'postcss-flexbugs-fixes',
            [
              'postcss-preset-env',
              {
                autoprefixer: {
                  flexbox: 'no-2009',
                },
                stage: 3,
              },
            ],
            // Adds PostCSS Normalize as the reset css with default options,
            // so that it honors browserslist config in package.json
            // which in turn let's users customize the target behavior as per their needs.
            'postcss-normalize',
          ]
        },
        sourceMap: isEnvProduction ? shouldUseSourceMap : isEnvDevelopment,
      },
    },
  ].filter(Boolean);
  return loaders;
};

const getConfig = (target) => {
  return {
    entry: "./src/index.js",
    target: target === "web" ? "web" : "node",
    mode: NODE_ENV,
    devtool: "hidden-source-map",
    ...(!isDevServer && {
      output: {
        path: path.resolve(__dirname, "dist", target),
        publicPath: `${PUBLIC_URL}/${target}/`,
        clean: true,
      },
    }),
    resolve: {
      extensions: [
        ".jsx",
        ".js",
        ".json",
        ".css",
        ".scss",
        ".jpg",
        "jpeg",
        "png",
      ],
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx|ts|tsx|mjs)$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                babelrc: true,
                compact: isEnvProduction,
                cacheDirectory: true,
                cacheCompression: false,
                babelrcRoots: __dirname,
              },
            },
          ],
        },
        {
          test: cssRegex,
          exclude: cssModuleRegex,
          use: getStyleLoaders({
            importLoaders: 1,
            sourceMap: isEnvProduction
              ? shouldUseSourceMap
              : isEnvDevelopment,
            modules: {
              mode: 'icss',
            },
          }),
          // Don't consider CSS imports dead code even if the
          // containing package claims to have no side effects.
          // Remove this when webpack adds a warning or an error for this.
          // See https://github.com/webpack/webpack/issues/6571
          sideEffects: true,
        },
        // Adds support for CSS Modules (https://github.com/css-modules/css-modules)
        // using the extension .module.css
        {
          test: cssModuleRegex,
          use: getStyleLoaders({
            importLoaders: 1,
            sourceMap: isEnvProduction
              ? shouldUseSourceMap
              : isEnvDevelopment,
            modules: {
              mode: 'local',
              getLocalIdent: getCSSModuleLocalIdent,
            },
          }),
        },
        {
          test: /\.(gif|svg|png|jpe?g)$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                limit: 1024,
                name: '[name].[ext]',
                emitFile: target === 'web',
              },
            },
          ],
        },
      ],
    },
    plugins: [
      new webpack.ProgressPlugin(webpackProgressHandler),
      new Dotenv({ defaults: false, path: './.env', expand: true }),
      isEnvProduction && new MiniCssExtractPlugin({
        // Options similar to the same options in webpackOptions.output
        // both options are optional
        filename: 'css/[name].[contenthash:8].css',
        chunkFilename: 'css/[name].[contenthash:8].chunk.css',
      }),
      new ModuleFederationPlugin({
        name: "Product",
        filename: "remoteEntry.js",
        exposes: {
          "./App": "./src/App",
        },
        ...(!isDevServer && {
          shared: {
            react: {
              eager: false,
              singleton: true,
              requiredVersion: packageJson.dependencies["react"],
            },
            ["react-dom"]: {
              eager: false,
              singleton: true,
              requiredVersion: packageJson.dependencies["react-dom"],
            },
            ["react-router-dom"]: {
              eager: false,
              singleton: true,
            },
          },
        })
      }),
      ...(target === "web"
        ? [
            new HtmlWebpackPlugin(
              Object.assign(
                {},
                {
                  inject: true,
                  template: "./public/index.html",
                },
                isEnvProduction
                  ? {
                      minify: {
                        removeComments: true,
                        collapseWhitespace: true,
                        removeRedundantAttributes: true,
                        useShortDoctype: true,
                        removeEmptyAttributes: true,
                        removeStyleLinkTypeAttributes: true,
                        keepClosingSlash: true,
                        minifyJS: true,
                        minifyCSS: true,
                        minifyURLs: true,
                      },
                    }
                  : undefined
              )
            ),
            new InterpolateHtmlPlugin(HtmlWebpackPlugin, { PUBLIC_URL })
          ]
        : [new NodeAsyncHttpRuntime()]),
    ].filter(Boolean),
  }
};

module.exports = [getConfig("web"), getConfig("node")];