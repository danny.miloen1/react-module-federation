import React from "react";
import { Link } from "react-router-dom";

import styles from './styles.module.css';

const Home = () => {
  return (
    <div>
      <div className={styles.title}>Product Homes Page</div>
    </div>
  );
};

export default Home;