import React from "react";
import { Outlet, Link } from "react-router-dom";

import styles from './styles.module.css';

const Layout = () => {
  return (
    <div className={styles.layout}>
      <div className={styles['menu-title']}>Menu:</div>
      <div className={styles['product-section']}>
        <Link to="/stocks">
          <div className={styles['product-card']}>Stocks</div>
        </Link>
        <Link to="/reksadana">
          <div className={styles['product-card']}>Reksadana</div>
        </Link>
      </div>
      <Outlet />
    </div>
  );
};

export default Layout;