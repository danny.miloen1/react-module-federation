import React from 'react';
import { Routes, Route } from "react-router-dom";

const Layout = React.lazy(() => import(/* webpackChunkName: "Product_Layout" */ '../features/layout'));
const NotFound = React.lazy(() => import(/* webpackChunkName: "Product_NotFound" */ '../features/notfound'));

const Home = React.lazy(() => import(/* webpackChunkName: "Product_Home" */ '../features/home'));
const Stocks = React.lazy(() => import(/* webpackChunkName: "Product_Stocks" */ '../features/stocks'));
const Reksadana = React.lazy(() => import(/* webpackChunkName: "Product_Reksadana" */ '../features/reksadana'));

const AppRoute = () => {
 return (
  <Routes>
    <Route path="/" element={<Layout />}>
      <Route index element={<Home />} />
      <Route
        path="stocks"
        element={
          <React.Suspense fallback={<>...</>}>
            <Stocks />
          </React.Suspense>
        }
      />
      <Route
        path="reksadana"
        element={
          <React.Suspense fallback={<>...</>}>
            <Reksadana />
          </React.Suspense>
        }
      />
      <Route path="*" element={<NotFound />} />
    </Route>
  </Routes>
 );
};

export default AppRoute;
