import React from "react";
import { BrowserRouter, } from "react-router-dom";

import Routes from './routes/index';

const App = () => {
  return (
    <React.StrictMode>
      <BrowserRouter basename="product">
        <Routes />
      </BrowserRouter>
    </React.StrictMode>
  )
}

export default App;

