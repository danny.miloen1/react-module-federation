import React, { Suspense } from 'react';
import dynamic from 'next/dynamic'

const ProductAppService = dynamic(() => import('Product/App'), { suspense: false, ssr: false })

const Product = () => (
  <Suspense fallback={<div>Loading...</div>}>
    <ProductAppService />
  </Suspense>
);
  
export default Product;
  