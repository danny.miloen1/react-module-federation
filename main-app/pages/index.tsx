import Link from 'next/link'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <main className={styles.main}>
        <div className={styles.title}>Main App</div>
        <div>Menu:</div>
        <Link href="/product">
          <div className={styles.menu}>Product</div>
        </Link>
        <Link href="/portofolio">
          <div className={styles.menu}>Portofolio</div>
        </Link>
      </main>
    </div>
  )
}