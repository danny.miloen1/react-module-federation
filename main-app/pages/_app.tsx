import React from "react";
import Head from "next/head";

import appConfig from '../config/app';

import "../styles/globals.css";

globalThis.React = React;

function MyApp({ Component, pageProps }: any) {
  return (
    <>
      <Head>
        {Object.keys(appConfig.appServices).map((config, idx) => (
          // @ts-ignore
          <script key={idx} async src={`${appConfig.appServices[config]}/web/remoteEntry.js`} /> 
        ))}
      </Head>
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
