import React, { Suspense } from 'react';
import dynamic from 'next/dynamic'

const PortofolioAppService = dynamic(() => import('Portofolio/App'), { suspense: false, ssr: false })

const Portofolio = () => (
  <Suspense fallback={<div>Loading...</div>}>
    <PortofolioAppService />
  </Suspense>
);

export default Portofolio;
  