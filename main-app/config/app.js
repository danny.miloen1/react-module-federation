const appServices = {
  Product: process.env.PRODUCT_SERVICE_URL,
  Portofolio: process.env.PORTOFOLIO_SERVICE_URL
}

module.exports = {
  appServices
}