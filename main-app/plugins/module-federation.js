const packageJsonDeps = require("../package.json").dependencies;
const appConfig = require('../config/app');

const rpcPerformTemplate = `
  function rpcLoad() {
    var url;
    var cb = arguments[arguments.length - 1];
    if (typeof cb !== "function") {
        throw new Error("last argument should be a function");
    }
    if (arguments.length === 2) {
        url = arguments[0];
    } else if (arguments.length === 3) {
        url = new URL(arguments[1], arguments[0]).toString();
    } else {
        throw new Error("invalid number of arguments");
    }

    let request = (url.startsWith('https') ? require('https') : require('http')).get(url, function(resp) {
        if (resp.statusCode === 200) {
            let rawData = '';
            resp.setEncoding('utf8');
            resp.on('data', chunk => { rawData += chunk; });
            resp.on('end', () => {
                cb(null, rawData);
            });
        } else {
            cb(resp);
        }
    });
    request.on('error', error => cb(error));
  }

  function rpcPerform(remoteUrl) {
      const scriptUrl = remoteUrl.split("@")[1];
      const moduleName = remoteUrl.split("@")[0];
      return new Promise(function (resolve, reject) {
          rpcLoad(scriptUrl, function(error, scriptContent) {
              if (error) { reject(error); }
              //TODO using vm??
              const remote = eval(scriptContent + '\\n  try{' + moduleName + '}catch(e) { null; };');
              if (!remote) {
                reject("remote library " + moduleName + " is not found at " + scriptUrl);
              } else if (remote instanceof Promise) {
                  return remote;
              } else {
                  resolve(remote);
              }
          });
      });
  }
`;

const rpcProcessTemplate = (conf) => `
    function rpcProcess(remote) {
        return {get:(request)=> remote.get(request),init:(arg)=>{try {return remote.init({
            ...arg,
            ${Object.keys(conf.shared || {})
              .filter(
                (item) =>
                  conf.shared[item].singleton &&
                  conf.shared[item].requiredVersion
              )
              .map(function (item) {
                return `"${item}": {
                      ["${conf.shared[item].requiredVersion}"]: {
                        get: () => Promise.resolve().then(() => () => require("${item}"))
                      }
                  }`;
              })
              .join(",")}
        })} catch(e){console.log('remote container already initialized')}}}
    }
`;

function buildRemotes(mfConf, getRemoteUri) {
  const builtinsTemplate = `
    ${rpcPerformTemplate}
    ${rpcProcessTemplate(mfConf)}
  `;
  return Object.entries(mfConf.remotes || {}).reduce((acc, [name, config]) => {
    acc[name] = {
      external: `external (async function() {
        ${builtinsTemplate}
        return rpcPerform(${
          getRemoteUri
            ? `await ${getRemoteUri(config)}`
            : `"${config}"`
        }).then(rpcProcess).catch((err) => { console.error(err); })
      }())`,
    };
    return acc;
  }, {});
}

class NodeModuleFederation {
  constructor(options, context) {
    this.options = options || {};
    this.context = context || {};
  }

  apply(compiler) {
    const { getRemoteUri, ...options } = this.options;
    // When used with Next.js, context is needed to use Next.js webpack
    const { webpack } = this.context;

    new (webpack?.container.ModuleFederationPlugin ||
      require("webpack/lib/container/ModuleFederationPlugin"))({
      ...options,
      remotes: buildRemotes(options, getRemoteUri),
    }).apply(compiler);
  }
}

function mfConfig ({ isServer }) {
  const appRemotes = {};

  Object.keys(appConfig.appServices).forEach(cfg => {
    appRemotes[cfg] = isServer
    ? `${cfg}@${appConfig.appServices[cfg]}/node/remoteEntry.js`
    : {
      external: `external new Promise((r, j) => {
        window['${cfg}'].init({
          react: {
            "${packageJsonDeps.react}": {
              get: () => Promise.resolve().then(() => () => globalThis.React),
            }
          }
        });
        r({
          get: (request) => window['${cfg}'].get(request),
          init: (args) => {}
        });
      })`
    }
  })

  return { remotes: appRemotes }
}



module.exports = {
  NodeModuleFederation,
  mfConfig
}