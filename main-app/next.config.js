/** @type {import('next').NextConfig} */

const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const { NodeModuleFederation, mfConfig } = require('./plugins/module-federation');

module.exports = {
  future: { webpack5: true },
  swcMinify: true,
  reactStrictMode: true,
  env: {
    PRODUCT_SERVICE_URL: process.env.PRODUCT_SERVICE_URL,
    PORTOFOLIO_SERVICE_URL: process.env.PORTOFOLIO_SERVICE_URL,
  },
  webpack: (config, options) => {
    const { isServer, dev } = options;

    const MFConfig = mfConfig({ isServer });
    const MFPlugin = new (isServer ? NodeModuleFederation : ModuleFederationPlugin)(MFConfig);
   
    return {
      ...config,
      ...(dev ? { experiments: { topLevelAwait: true } } : {}),
      plugins: [
        ...config.plugins,
        MFPlugin,
      ],
    };
  },
};
