import React from 'react';
import { Routes, Route } from "react-router-dom";

const Layout = React.lazy(() => import(/* webpackChunkName: "Portofolio_Layout" */ '../features/layout'));
const NotFound = React.lazy(() => import(/* webpackChunkName: "Portofolio_NotFound" */ '../features/notfound'));

const Home = React.lazy(() => import(/* webpackChunkName: "Home" */ '../features/home'));
const DanaTabungan = React.lazy(() => import(/* webpackChunkName: "Portofolio_DanaTabungan" */'../features/danatabungan'));
const DanaPensiun = React.lazy(() => import(/* webpackChunkName: "Portofolio_DanaPensiun" */ '../features/danapensiun'));

const AppRoute = () => {
 return (
  <Routes>
    <Route path="/" element={<Layout />}>
      <Route index element={<Home />} />
      <Route
        path="dana-tabungan"
        element={
          <React.Suspense fallback={<>...</>}>
            <DanaTabungan />
          </React.Suspense>
        }
      />
      <Route
        path="dana-pensiun"
        element={
          <React.Suspense fallback={<>...</>}>
            <DanaPensiun />
          </React.Suspense>
        }
      />
      <Route path="*" element={<NotFound />} />
    </Route>
  </Routes>
 );
};

export default AppRoute;
