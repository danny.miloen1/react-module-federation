import React from "react";
import { Outlet, Link } from "react-router-dom";

import styles from './styles.module.css';

const Layout = () => {
  return (
    <div className={styles.layout}>
      <div className={styles['menu-title']}>Menu:</div>
      <div className={styles['portofolio-section']}>
        <Link to="/dana-tabungan">
          <div className={styles['portofolio-card']}>Dana Tabungan</div>
        </Link>
        <Link to="/dana-pensiun">
          <div className={styles['portofolio-card']}>Dana Pensiun</div>
        </Link>
      </div>
      <Outlet />
    </div>
  );
};

export default Layout;