const RuntimeGlobals = require("webpack/lib/RuntimeGlobals");
const StartupChunkDependenciesPlugin = require("webpack/lib/runtime/StartupChunkDependenciesPlugin");

const ChunkLoadingRuntimeModule = require("./LoadFileChunkLoadingRuntimeModule");


class CommonJsChunkLoadingPlugin {
  constructor(options, context) {
    this.options = options || {};
    this._asyncChunkLoading = this.options.asyncChunkLoading;
    this.context = context || {};
  }

  /**
   * Apply the plugin
   * @param {Compiler} compiler the compiler instance
   * @returns {void}
   */
  apply(compiler) {
    const chunkLoadingValue = this._asyncChunkLoading
      ? "async-node"
      : "require";
    new StartupChunkDependenciesPlugin({
      chunkLoading: chunkLoadingValue,
      asyncChunkLoading: this._asyncChunkLoading,
    }).apply(compiler);
    compiler.hooks.thisCompilation.tap(
      "CommonJsChunkLoadingPlugin",
      (compilation) => {
        // Always enabled
        const isEnabledForChunk = () => true;
        const onceForChunkSet = new WeakSet();
        const handler = (chunk, set) => {
          if (onceForChunkSet.has(chunk)) return;
          onceForChunkSet.add(chunk);
          if (!isEnabledForChunk(chunk)) return;
          set.add(RuntimeGlobals.moduleFactoriesAddOnly);
          set.add(RuntimeGlobals.hasOwnProperty);
          compilation.addRuntimeModule(
            chunk,
            new ChunkLoadingRuntimeModule(set, this.options, this.context)
          );
        };

        compilation.hooks.runtimeRequirementInTree
          .for(RuntimeGlobals.ensureChunkHandlers)
          .tap("CommonJsChunkLoadingPlugin", handler);
        compilation.hooks.runtimeRequirementInTree
          .for(RuntimeGlobals.hmrDownloadUpdateHandlers)
          .tap("CommonJsChunkLoadingPlugin", handler);
        compilation.hooks.runtimeRequirementInTree
          .for(RuntimeGlobals.hmrDownloadManifest)
          .tap("CommonJsChunkLoadingPlugin", handler);
        compilation.hooks.runtimeRequirementInTree
          .for(RuntimeGlobals.baseURI)
          .tap("CommonJsChunkLoadingPlugin", handler);
        compilation.hooks.runtimeRequirementInTree
          .for(RuntimeGlobals.externalInstallChunk)
          .tap("CommonJsChunkLoadingPlugin", handler);
        compilation.hooks.runtimeRequirementInTree
          .for(RuntimeGlobals.onChunksLoaded)
          .tap("CommonJsChunkLoadingPlugin", handler);

        compilation.hooks.runtimeRequirementInTree
          .for(RuntimeGlobals.ensureChunkHandlers)
          .tap("CommonJsChunkLoadingPlugin", (chunk, set) => {
            if (!isEnabledForChunk(chunk)) return;
            set.add(RuntimeGlobals.getChunkScriptFilename);
          });
        compilation.hooks.runtimeRequirementInTree
          .for(RuntimeGlobals.hmrDownloadUpdateHandlers)
          .tap("CommonJsChunkLoadingPlugin", (chunk, set) => {
            if (!isEnabledForChunk(chunk)) return;
            set.add(RuntimeGlobals.getChunkUpdateScriptFilename);
            set.add(RuntimeGlobals.moduleCache);
            set.add(RuntimeGlobals.hmrModuleData);
            set.add(RuntimeGlobals.moduleFactoriesAddOnly);
          });
        compilation.hooks.runtimeRequirementInTree
          .for(RuntimeGlobals.hmrDownloadManifest)
          .tap("CommonJsChunkLoadingPlugin", (chunk, set) => {
            if (!isEnabledForChunk(chunk)) return;
            set.add(RuntimeGlobals.getUpdateManifestFilename);
          });
      }
    );
  }
}

class NodeAsyncHttpRuntime {
  constructor(options, context) {
    this.options = options || {};
    this.context = context || {};
  }

  apply(compiler) {
    if (compiler.options.target) {
      console.warn(
        `target should be set to false while using NodeAsyncHttpRuntime plugin, actual target: ${compiler.options.target}`
      );
    }

    // When used with Next.js, context is needed to use Next.js webpack
    const { webpack } = this.context;

    // This will enable CommonJsChunkFormatPlugin
    compiler.options.output.chunkFormat = "commonjs";
    // This will force async chunk loading
    compiler.options.output.chunkLoading = "async-node";
    // Disable default config
    compiler.options.output.enabledChunkLoadingTypes = false;

    new (webpack?.node.NodeEnvironmentPlugin ||
      require("webpack/lib/node/NodeEnvironmentPlugin"))({
      infrastructureLogging: compiler.options.infrastructureLogging,
    }).apply(compiler);
    new (webpack?.node.NodeTargetPlugin ||
      require("webpack/lib/node/NodeTargetPlugin"))().apply(compiler);
    new CommonJsChunkLoadingPlugin(
      {
        asyncChunkLoading: true,
        baseURI: compiler.options.output.publicPath,
        getBaseUri: this.options.getBaseUri,
      },
      this.context
    ).apply(compiler);
  }
}

module.exports = NodeAsyncHttpRuntime;