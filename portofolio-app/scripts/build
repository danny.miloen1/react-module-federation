#!/usr/bin/env node

const fs = require('fs-extra');
const path = require('path');
const webpack = require('webpack');
const chalk = require('react-dev-utils/chalk');

require('./utils/env');

const formatTime = time => time.toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, '$1');

async function build() {
  await bundle();
  copyPublicFolder();
};

function bundle() {
  const webpackConfig = require('../webpack.config');

  const startTime = new Date();
  console.info(`[${formatTime(startTime)}] Starting ''...`);

  fs.emptyDirSync(path.resolve('.', 'dist'));

  return new Promise((resolve, reject) => {
    webpack(webpackConfig).run((err, stats) => {
      if (err) return reject(err);

      console.info(chalk.blueBright(stats.toString(webpackConfig[0].stats)));
      console.info(chalk.green(`[${formatTime(new Date())}] Finished bundle after ${new Date().getTime() - startTime.getTime()} ms`));

      return resolve();
    });
  });
};

function copyPublicFolder() {
  console.info(`[${formatTime(new Date())}] Copying public folder...`);

  fs.copySync(path.resolve('.', 'public'), path.resolve('.', 'dist', 'web'), {
    dereference: true,
    filter: file => file !== path.resolve('.', 'public/index.html'),
  });
};

if (require.main === module && process.argv.length > 1) {
  process.env.DEV_SERVER = 'false';

  delete require.cache[__filename];

  build().catch(err => {
    console.error(err.stack);
    process.exit(1)
  })
}